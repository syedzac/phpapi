<?php

class Data {

	const PATH = "./data/items.csv";
	
	public function checkFileExist() {
		if( file_exists(self::PATH) ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function pingResponse() {
		if( $this->checkFileExist() ) {
			$response['id'] = "200";	
			$response['message']= "System responded, ready to use.";		
		}else {
			$response['id'] = "500";	
			$response['message']= "Unsuccessful ping; the application is NOT ready.";
		}
		
		$json_response = json_encode($response);
		echo $json_response;
	} //EOF pingResponse()

	public function getItemsDataFromCsv() {
		
		$response = array();
		if ( file_exists(self::PATH) ) {
			$fileHandleRead = fopen(self::PATH, "r");
			while (($row = fgetcsv($fileHandleRead, 0, ",")) !== FALSE) {
				$arr['id'] = $row[0];
				$arr['name'] = $row[1];
				$arr['tax'] = $row[2];
				$arr['price'] = $row[3];
				$response[] = $arr;
			}
		}
		return $response;
	} //EOF getItemsDataFromCsv()

	public function getItemsArrayFromCsv() {
		$arr = array();
		if ( file_exists( self::PATH ) ) {
			$fileHandleRead = fopen(self::PATH, "r");
			while (($row = fgetcsv($fileHandleRead, 0, ",")) !== FALSE) {
				$arr[$row[0]]['name'] = $row[1];
				$arr[$row[0]]['tax'] = $row[2];
				$arr[$row[0]]['price'] = $row[3];
			}
		}
		return $arr;
	} //getItemsArrayFromCsv()


	public function itemsListResponse() {
		$response = $this->getItemsDataFromCsv();

		if ( count($response) == 0 ) {
			$response['id'] = "500";	
			$response['message']= "Unable to retrieve items.";
		}

		$json_response = json_encode($response);
		echo $json_response;
	} //EOF itemsListResponse()

	public function calculateInvoiceResponse( $postData ) {
		$arr = $this->getItemsArrayFromCsv();
		$responseTax = 0;	$responseTotal = 0;
		if (is_array($arr) AND count($arr) > 0) {
			foreach ($postData as $value) {
				if( is_array($value) ) {
					if ( array_key_exists($value['id'],$arr) ) {
						
						$item['id'] = $value['id'];
						$item['name'] = $arr[$value['id']]['name'];
						$item['quantity'] = $value['quantity'];
						$item['discount'] = $value['discount'];
						$item['tax_rate'] = $arr[$value['id']]['tax'];
						$item['total'] = $arr[$value['id']]['price'];

						if($item['discount'] > 0 AND $item['discount'] <= 50) {
							$response["lines"][] = $item;
							$itemsPrice = $item['total'] * $item['quantity'];

							$responseTax 	+= ( $itemsPrice * $item['tax_rate'] / 100 );
							$responseTotal 	+= ( $itemsPrice - ($item['discount'] / 100) * $itemsPrice );
						}else{
							$itemError['id'] = $value['id'];
							$itemError['message'] = "Discount can be no less than 0%, and no greater than 50%";

							$response["lines"][] = $itemError;
						} 
					}else{
						$itemError['id'] = $value['id'];
						$itemError['message'] = "Product not found in the list";

						$response["lines"][] = $itemError;
					}
				}
			}
		}else{
			$response['id'] = "500";	
			$response['message']= "Unable to retrieve items.";
		}
		
		$response['tax'] = $responseTax;	
		$response['total'] = $responseTotal + $responseTax;
		
		$json_response = json_encode($response);
		echo $json_response;
	} //EOF calculateInvoiceResponse()

	public function prettyPrint( $json ) {
	    $result = '';
	    $level = 0;
	    $in_quotes = false;
	    $in_escape = false;
	    $ends_line_level = NULL;
	    $json_length = strlen( $json );

	    for( $i = 0; $i < $json_length; $i++ ) {
	        $char = $json[$i];
	        $new_line_level = NULL;
	        $post = "";
	        if( $ends_line_level !== NULL ) {
	            $new_line_level = $ends_line_level;
	            $ends_line_level = NULL;
	        }
	        if ( $in_escape ) {
	            $in_escape = false;
	        } else if( $char === '"' ) {
	            $in_quotes = !$in_quotes;
	        } else if( ! $in_quotes ) {
	            switch( $char ) {
	                case '}': case ']':
	                    $level--;
	                    $ends_line_level = NULL;
	                    $new_line_level = $level;
	                    break;

	                case '{': case '[':
	                    $level++;
	                case ',':
	                    $ends_line_level = $level;
	                    break;

	                case ':':
	                    $post = " ";
	                    break;

	                case " ": case "\t": case "\n": case "\r":
	                    $char = "";
	                    $ends_line_level = $new_line_level;
	                    $new_line_level = NULL;
	                    break;
	            }
	        } else if ( $char === '\\' ) {
	            $in_escape = true;
	        }
	        if( $new_line_level !== NULL ) {
	            $result .= "\n".str_repeat( "\t", $new_line_level );
	        }
	        $result .= $char.$post;
	    }

	    return $result;
	} //prettyPrint( $json )

}
