# Setup and installation
This repository defines a simple API using [OpenAPI v3.0.1](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.1.md).

The full API documentation is available at [https://abss-engineering-php-challenge.netlify.com](https://abss-engineering-php-challenge.netlify.com)

The API spec is defined in `openapi.yml`. Each URL has its own endpoint in `endpoints/` and JSON request/response schemas are defined in `schemas/`. JSON Schema is used to define responses and can be used for validation.

## Setup and run the documentation
Run these commands to install npm dependencies and serve the API documentation locally.

```sh
npm install
npm run serve
```

The API documentation will be available at [https://localhost:5000](http://localhost:5000).


_To contribute to this API specification, read [DEVME.md](DEVME.md)._