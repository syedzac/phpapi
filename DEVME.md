# Development & Contribution guide
> This is intended for internal development purposes. It is not required to complete the php-challenge.


## Contributing to this API spec

This spec uses `speccy` to lint and build the completed OpenAPI definition.

```sh
npm run test    # run the speccy linter over openapi.yml
npm run publish # resolve all $ref's and publish a single YAML file in dist/openapi.yml
```
