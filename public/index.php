<?php

require_once('../helper/Data.php');

$helper = new Data(); 

$data = '{
"lines": [
    {
      "id": "c2f5b5b6-d85d-41bb-9651-bea30cc880b0",
      "quantity": 1,
      "discount": 70
    },
    {
      "id": "8439f607-c4b9-42d1-bbb0-2aa89d1edc72",
      "quantity": 2,
      "discount": 5
    },
    {
      "id": "f2cbada8-7b0e-45a1-a2d7-b0a27a85efda",
      "quantity": 1,
      "discount": 5
    }
  ]
}';

$url = "http://localhost/phpapi/endpoints/invoice.php";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$server_output = curl_exec ($ch);

curl_close ($ch);

//echo "<pre>"; print_r($server_output);  echo "</pre>";
echo "<pre>"; print_r($helper->prettyPrint($server_output));  echo "</pre>";

echo $helper->checkFileExist();

?>