<?php

header("Content-Type:application/json");

require_once('../helper/Data.php');

$helper = new Data(); 

$data = json_decode(file_get_contents('php://input'), true);	// Get the request parameters 

if(!empty($data['lines'])) {
	$helper->calculateInvoiceResponse($data['lines']);
}else{
	$response['id'] = "400";	
	$response['message']= "Request data required.";

	$json_response = json_encode($response);
	echo $json_response;
}